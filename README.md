# Use MVVM C#
## _The Last Markdown Editor, Ever_

## Model: 
The model layer is the layer that contains the business logic, and it gets and saves the details from the data source for using up by the ViewModel.
## ViewModel: 
The view layer is the intermediate between the Model and View, and it will not transform the raw information from the Model into the fit-to-be-seen structure for the View. Let’s see one example of the conversion, like the Boolean flag from the model to the string of true or false for the view.
## View: 
It is the layer that represents the interface of the software, like the Graphical User Interface (GUI). It will display the information from ViewModel to the client and talk the changes of details back to the ViewModel. It acts as the link/connection between the Model and ViewModel and makes stuff look pretty.

The three components act as a team by referencing each other in the following pattern as follows:

- 	View points out the ViewModel
- 	ViewModel points out the Model

The essential thing is that the ViewModel and the View are able to communicate in two methods called Data Bindings. The foremost component for communication is the interface called INotifyPropertyChanged.
To make use of this method, the View must alter the information in the ViewModel all the way through the client input, and the ViewModel must update the View with the information which has been updated through the processes in the Model or to updated information from the repository. The MVVM (Model View ViewModel) architecture places high prominence on the Separation of Concerns for each and every layer. By separating layers, there are some more benefits. Let’s see the following things.

-	Modularity: The modularity supports that it has been altered or exchanged during the layers internal implementation without disturbing others.
-	Increased Testability: In this, each and every component must be tested by a unit test with false information, and it is impossible if the ViewModel program is written in Code-Behind of View.

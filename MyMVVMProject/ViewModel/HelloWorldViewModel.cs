﻿using MyMVVMProject.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MyMVVMProject.ViewModel
{
    public class HelloWorldViewModel : INotifyPropertyChanged
    {
        private string _helloString;
        public event PropertyChangedEventHandler PropertyChanged;
        public string HelloString
        {
            get
            {
                return _helloString;
            }
            set
            {
                _helloString = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// when the Property modifies it Raises OnPropertychangedEvent
        /// </summary>
        /// <param name="name">Property name represented by String</param>
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        public HelloWorldViewModel()
        {
            HelloWorldModel hwModel1 = new HelloWorldModel();
            _helloString = hwModel1.ImportantInfo;
        }
    }
}
